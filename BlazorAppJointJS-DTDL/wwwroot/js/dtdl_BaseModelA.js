﻿
export function basemodelA() {
    var iheigtht = 600;
    var graph = new joint.dia.Graph;
    var paper = new joint.dia.Paper({
        el: $('#myholder'), // HTML上div ID
        model: graph,
        width: 700,         // 画布宽
        height: iheigtht,        // 画布宽
        gridSize: 5,        // 图形元素移动步进像素
        drawGrid: true,     // 显示步进点
        background: {       // 画布背景色
            color: 'rgba(0, 0, 0, 0.1)'
        },
        // 连接线风格
        defaultLink: new joint.shapes.logic.Wire({
            connector: { name: 'jumpover' },  // 当连线交叉时，跳过
        }),
        linkPinning: false,   // 不允许连线到空白处
        // 距离节点连接点25像素时自动连接上
        snapLinks: {
            radius: 25
        }
        ,
        //// 验证连线是否允许
        //validateConnection: function (viewSource, magnetSource, viewTarget, magnetTarget, end, viewLink) {
        //    if (end === 'target') {
        //        // 连线目标必须时一个"in"类型连接点
        //        if (!magnetTarget || !magnetTarget.getAttribute('port-group') || magnetTarget.getAttribute('port-group').indexOf('in') < 0) {
        //            return false;
        //        }


        //        // 检查连接点是否已经有线接入
        //        var portUsed = this.model.getLinks().some(function (link) {
        //            return (link.id !== viewLink.model.id &&
        //                link.get('target').id === viewTarget.model.id &&
        //                link.get('target').port === magnetTarget.getAttribute('port'));
        //        });

        //        return !portUsed;

        //    } else { // end === 'source'
        //        // 连线起始点必须时一个"out"类型连接点
        //        return magnetSource && magnetSource.getAttribute('port-group') && magnetSource.getAttribute('port-group').indexOf('out') >= 0;
        //    }
        //},
        
    });


    // 创建基础元件模板
    var gateTemplate = new joint.shapes.devs.Model({
        position: {
            x: 0,
            y: 0
        },
        size: {
            width: 110,
            height: 60
        },
        //portMarkup: '<rect class="joint-port-body" width="10" height="3" style="fill:black" />',
        //portLabelMarkup: '<text class="port-label joint-port-label" font-size="10" y="0" fill="#000" /> ',
        //ports: {
        //    groups: {
        //        'in': {
        //            attrs: {
        //                '.port-body': {
        //                    magnet: 'passive',
        //                },
        //                '.joint-port-body': {
        //                    x: -10
        //                    //y: -10
        //                }
        //            },
        //            label: {
        //                position: {
        //                    args: { x: 18 },
        //                },
        //            },
        //        }
        //        ,
        //        'out': {
        //            label: {
        //                position: {
        //                    args: { x: -23 },
        //                },
        //            },
        //        }
        //    }
        //},
        attrs: {
            '.label': {
                'type': 'primary',
                fontSize: 14,
                'ref-x': .5,
                //'ref-y': .05
                'ref-y': .1
            },
        }
    });

    function genInterfacePr() {
        //return gateTemplate.clone().set('inPorts', ['in1', 'in2']).set('outPorts', ['out']).attr('.label/text', 'Interface');
        return gateTemplate.clone().attr('.label/text', 'Interface').attr('role', 'Interface');
    }
    function genInterface() {
        return genInterfacePr().set('portMarkup', '<rect class="port-body joint-port-body" width="10" height="2" style="fill:black" />').attr('.label/type', 'instance')
    }

    function genPropertyPr() {
        return gateTemplate.clone().attr('.label/text', 'Property').attr('role', 'Property').attr('./fill', '#f2e6ff');
    }
    function genProperty() {
        return genPropertyPr().set('portMarkup', '<rect class="port-body joint-port-body" width="10" height="2" style="fill:black" />').attr('.label/type', 'instance').attr('.label/text', nodeName).attr('DataType', 'Boolean');
    }

    function genTelemetryPr() {
        return gateTemplate.clone().attr('.label/text', 'Telemetry').attr('role', 'Telemetry').attr('./fill', '#faebf5');
    }
    function genTelemetry() {
        return genTelemetryPr().set('portMarkup', '<rect class="port-body joint-port-body" width="10" height="2" style="fill:black" />').attr('.label/type', 'instance').attr('.label/text', nodeName).attr('DataType', 'Boolean');
    }

    function genRelationshipPr() {
        return gateTemplate.clone().attr('.label/text', 'Relationship').attr('role', 'Relationship').attr('./fill', '#f9f2ec');
    }
    function genRelationship() {
        return genRelationshipPr().set('portMarkup', '<rect class="port-body joint-port-body" width="10" height="2" style="fill:black" />').attr('.label/type', 'instance').attr('.label/text', nodeName).attr('maxMulti', '1').attr('minMulti', '1');
    }

    function genComponentPr() {
        return gateTemplate.clone().attr('.label/text', 'Component').attr('role', 'Component').attr('./fill', '#e6fff7');
    }
    function genComponent() {
        return genComponentPr().set('portMarkup', '<rect class="port-body joint-port-body" width="10" height="2" style="fill:black" />').attr('.label/type', 'instance').attr('.label/text', nodeName).attr('DataType', 'Boolean');
    }

    var i_Interface = 0;
    var i_Property = 0;
    var i_Telemetry = 0;
    var i_Relationship = 0;
    var i_Component = 0;
    var nodeName = "";
    var focusElement;
    var focusElement_pre;
    var delButton = new joint.shapes.standard.TextBlock();
    delButton.resize(10, 10);
    delButton.position(0, 0);
    delButton.attr('label/text', 'X');

    paper.on({
        // 'blank:pointerdown': function(elementView, evt) {
        //     if (this.currentEle) {
        //       console.log(this.currentEle);
        //       this.currentEle.unhighlight();
        //       this.currentEle = null;
        //     }
        // },

        'element:pointerdown': function (elementView, evt) {
            // if (this.currentEle) {
            //   console.log(this.currentEle);
            //   this.currentEle.unhighlight();
            //   this.currentEle = null;
            // }
            if (elementView.model.attr('.label/type') == 'primary') {
                var type = elementView.model.attr('.label/text');
                switch (type) {
                    case "Interface": graph.addCell(genInterfacePr().translate(20, 20)); break;
                    case "Property": graph.addCell(genPropertyPr().translate(20, 100)); break;
                    case "Telemetry": graph.addCell(genTelemetryPr().translate(20, 180)); break;
                    case "Relationship": graph.addCell(genRelationshipPr().translate(20, 260)); break;
                    case "Component": graph.addCell(genComponentPr().translate(20, 340)); break;
                }

                // 挪到图层的上层
                elementView.model.toFront();
            } else if (elementView.model.attr('.label/type') == 'instance') {
                evt.data = elementView.model.position();

                focusElement = elementView.model;

                document.getElementById("nodeName").value = focusElement.attr('.label/text');

                var type = elementView.model.attr('role');
                switch (type) {
                    case "Property":
                    case "Telemetry":
                        document.getElementById("dataType").value = focusElement.attr('DataType');
                        break;
                    case "Relationship":
                        document.getElementById("maxMulti").value = focusElement.attr('maxMulti');
                        document.getElementById("minMulti").value = focusElement.attr('minMulti');
                        break;
                    case "Component":
                        break;
                }

                if (focusElement_pre != null)
                    focusElement_pre.attr('.label/fill', '#000');   //取消原Focus

                focusElement.attr('.label/fill', '#06F');

                document.getElementById("dataType").disabled = (type == "Property" || type == "Telemetry")? false : true;
                document.getElementById("maxMulti").disabled = (type == "Relationship") ? false : true;
                document.getElementById("minMulti").disabled = (type == "Relationship") ? false : true;

                document.getElementById("role").value = type;

                // elementView.highlight();
                // this.currentEle = elementView;
            }
        },

        'element:pointerup': function (elementView, evt, x, y) {
            if (elementView.model.attr('.label/type') == 'primary') {
                if (elementView.model.position().x > 105) {
                    var type = elementView.model.attr('.label/text');
                    if (type == 'Interface') {
                        if (i_Interface == 0) {
                            graph.addCell(genInterface().translate(elementView.model.position().x, elementView.model.position().y));
                            i_Interface = i_Interface + 1;

                            genModelInfo("Interface");

                            document.getElementById("ToJSON").click();    //自動點選button
                        }

                    }
                    else if (i_Interface > 0) {

                        switch (type) {
                            case "Property":
                                i_Property = i_Property + 1;
                                nodeName = "Property_" + i_Property;
                                var cell = graph.addCell(genProperty().translate(elementView.model.position().x, elementView.model.position().y));
                                break;
                            case "Telemetry":
                                i_Telemetry = i_Telemetry + 1;
                                nodeName = "Telemetry_" + i_Telemetry;
                                var cell = graph.addCell(genTelemetry().translate(elementView.model.position().x, elementView.model.position().y));
                                break;
                            case "Relationship":
                                i_Relationship = i_Relationship + 1;
                                nodeName = "Relationship_" + i_Relationship;
                                var cell = graph.addCell(genRelationship().translate(elementView.model.position().x, elementView.model.position().y));
                                break;
                            case "Component":
                                i_Component = i_Component + 1;
                                nodeName = "Component_" + i_Component;
                                var cell = graph.addCell(genComponent().translate(elementView.model.position().x, elementView.model.position().y));
                                break;
                        }

                        document.getElementById("ToJSON").click();    //自動點選button
                    }
                    else
                        alert("Please add [Interface] first.")
                }
                graph.removeCells(elementView.model);

                    if (elementView.model.position().x > 105) {
                        //var type = elementView.model.attr('.label/text');
                        var type = elementView.model.attr('role');
                        var source;

                        if (type == 'Property' || type == 'Telemetry' || type == 'Relationship' || type == 'Component') {
                        //---建立 interface與各child的連線---
                        graph.getElements().forEach(function (el) {
                            if (el.attr('role') == 'Interface' && el.attr('.label/type') == 'instance') {
                                source = el;

                            }
                        });

                        graph.getElements().forEach(function (el) {
                            if (el.attr('role') != 'Interface' && el.attr('.label/type') == 'instance') {
                                //var link = new joint.shapes.standard.Link();  //---線終點有箭頭
                                var link = new joint.dia.Link();    //---線終點無箭頭
                                link.source(source);
                                link.target(el);
                                link.addTo(graph);
                            }
                        });

                        
                    }
                }

            } else {

                focusElement_pre = focusElement;

                if (elementView.model.position().x < 110) {
                    elementView.model.position(evt.data.x, evt.data.y);
                }
            }
        },

        'element:mouseover': function (elementView, evt) {

            if (elementView.model.attr('.label/type') == 'instance') {

                console.log(elementView);
                //elementView.highlight();
                elementView.model.embed(delButton.clone().translate(elementView.model.position().x, elementView.model.position().y));

            }
        },

        //'element:mouseclick': function (elementView, evt) {

        //    alert(elementView.mode.data.value);
        //},

        'element:mouseout': function (elementView, evt) {
            if (elementView.model.attr('.label/type') == 'instance') {
                elementView.unhighlight();


            }
        },

        'element:pointerdblclick': function (elementView, evt) {
            if (elementView.model.attr('.label/type') == 'instance') {

                if (elementView.model.attr('.label/text') != 'Interface')
                {
                    evt.data = elementView.model.position();
                    elementView.model.remove();
                }

                document.getElementById("ToJSON").click();    //自動點選button
            }
        },

    })

    graph.addCell(genInterfacePr().translate(20, 20));
    graph.addCell(genPropertyPr().translate(20, 100));
    graph.addCell(genTelemetryPr().translate(20, 180));
    graph.addCell(genRelationshipPr().translate(20, 260));
    graph.addCell(genComponentPr().translate(20, 340));

    var separator = new joint.shapes.standard.Polyline();
    separator.resize(5, iheigtht);
    separator.position(150, 0);
    separator.addTo(graph);


    //-------------------------------------------------------------------
    //document.getElementById("mySavedModel").value = JSON.stringify(graph.toJSON());

    document.getElementById("Set").addEventListener("click", function ()
    {
        focusElement.attr('.label/text', document.getElementById("nodeName").value);

        var type = focusElement.attr("role");
        switch (type)
        {
            case "Interface":
                genModelInfo(focusElement.attr('.label/text'));
                //genModelInfo(document.getElementById("model_name").value);
                break;

            case "Property":
            case "Telemetry":
                focusElement.attr('DataType', document.getElementById("dataType").value);
                break;

            case "Relationship":
                var maxMulti = parseInt(document.getElementById("maxMulti").value);
                var minMulti = parseInt(document.getElementById("minMulti").value);

                if (minMulti > maxMulti)
                    alert("The [maxMulti] must be gte [minMulti].");
                else
                {
                    focusElement.attr('maxMulti', maxMulti);
                    focusElement.attr('minMulti', minMulti);
                }
                break;
        }

        document.getElementById("ToJSON").click();    //自動點選button
    });


    document.getElementById("ToJSON").addEventListener("click", function () {
        //document.getElementById("JSON").value = JSON.stringify(graph.toJSON());


        var Object_Property = [];
        var Object_Telemetry = [];
        var Object_Relationship = [];
        graph.getElements().forEach(function (el) {

            if (el.attr('.label/type') == 'instance')
            {
                var type = el.attr("role");
                switch (type)
                {
                    case "Interface":
                        break;

                    case "Property":
                        var pro = new Property(el.attr('DataType'), el.attr('.label/text'));
                        Object_Property.push(pro);
                        break;
                    case "Telemetry":
                        var pro = new Telemetry(el.attr('DataType'), el.attr('.label/text'));
                        Object_Telemetry.push(pro);
                        break;

                    case "Relationship":
                        var maxMulti = parseInt(document.getElementById("maxMulti").value);
                        var minMulti = parseInt(document.getElementById("minMulti").value);

                        var rel = new Relationship(maxMulti, minMulti, el.attr('.label/text'));
                        Object_Relationship.push(rel);
                        break;
                }
            }
        });


        const basemodel = new BaseModel(document.getElementById("model_id").value, 'Interface');
        basemodel.contents.push(Object_Property);
        basemodel.contents.push(Object_Telemetry);
        basemodel.contents.push(Object_Relationship);

        var json = JSON.stringify(basemodel);
        json = json.replace("\"id", "\"@id");

        //document.getElementById("JSON").value = JSON.stringify(json);

        document.getElementById("JSON").value = JSON.stringify(basemodel);
    });
}

function genModelInfo(name)
{
    document.getElementById("model_name").value = name;
    document.getElementById("model_id").value = "dtmi:com:example:" + name + ";1";
}

class BaseModel {
    constructor(id, type) {
        this.id = id
        this.type = type
        this.context = "dtmi:dtdl:context:2"
        this.comment= "-"
        this.description = "-"
        this.displayName = ""
        this.contents = []
        this.extends = []
        this.schemas = []
    }
}

class Property {
    constructor(schema, name) {
        this.type = "Property"
        this.name = name
        this.schema = schema
        this.id = ""
        this.comment = "-"
        this.description = "-"
        this.displayName = ""
    }
}

class Telemetry {
    constructor(schema, name) {
        this.schema = schema
        this.type = "Telemetry"
        this.name = name

    }
}

class Relationship {
    constructor(maxMulti, minMulti, name) {
        this.type = "Relationship"
        this.name = name
        this.maxMulti = maxMulti
        this.minMulti = minMulti
        this.target = "dtmi:com:example:Floor;1"
    }
}

