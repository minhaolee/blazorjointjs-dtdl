export function init_sample_DTDL_model() {
    let prevSelectElement = null;
    const stencilContainerGraph = new joint.dia.Graph;
    const stencilContainerPaper = new joint.dia.Paper({
        el: $('#stencilContainerHolder'),
        model: stencilContainerGraph,
        width: "100%",
        height: "100%",
        gridSize: 5,
        drawGrid: false,
        background: {
            color: '#6495ed',
        },
        interactive: false,
    });

    const graph = new joint.dia.Graph;
    const paper = new joint.dia.Paper({
        el: $('#myholder'), // HTML上div ID
        model: graph,
        width: "100%",
        height: "100%",
        gridSize: 5,        // 圖形元素移動步進像素
        drawGrid: true,     // 顯示步進點
        background: {       // 畫布被景色
            color: 'rgba(0, 0, 0, 0.1)'
        },
        // 連接線風格
        defaultLink: new joint.shapes.logic.Wire({
            connector: { name: 'jumpover' },  // 接線交叉時跳過
        }),
        linkPinning: false,   // 不允許接到空白
        // 距離節點連接點25像素時自動連上
        snapLinks: {
            radius: 25
        },
        // 驗證連線是否允許
        validateConnection: function (viewSource, magnetSource, viewTarget, magnetTarget, end, viewLink) {
            if (end === 'target') {
                // 连线目标必须时一个"in"类型连接点
                if (!magnetTarget || !magnetTarget.getAttribute('port-group') || magnetTarget.getAttribute('port-group').indexOf('in') < 0) {
                    return false;
                }

                // 检查连接点是否已经有线接入
                const portUsed = this.model.getLinks().some((link) => {
                    return (link.id !== viewLink.model.id &&
                        link.get('target').id === viewTarget.model.id &&
                        link.get('target').port === magnetTarget.getAttribute('port'));
                });

                return !portUsed;

            } else { // end === 'source'
                // 连线起始点必须时一个"out"类型连接点
                return magnetSource && magnetSource.getAttribute('port-group') && magnetSource.getAttribute('port-group').indexOf('out') >= 0;
            }
        },
    });

    const gateTemplate = new joint.shapes.devs.Model({
        position: {
            x: 0,
            y: 0
        },
        size: {
            width: 100,
            height: 80
        },
        // portMarkup: '<rect class="joint-port-body" width="10" height="3" style="fill:black" />',
        // portMarkup: '<rect class="joint-port-body" width="3" height="10" style="fill:black" />',
        // portLabelMarkup: '<text class="port-label joint-port-label" font-size="10" y="0" fill="#000" /> ',
        ports: {
            groups: {
                'in': {
                    position: {
                        name: "top",
                    },
                    attrs: {
                        '.port-body': {
                            magnet: 'passive',
                            r: 5,
                            stroke: "#212121",
                            fill: "#ffffff",
                        },
                        '.port-label': {
                            fontSize: 12,
                        },
                        '.joint-port-body': {
                            y: -10
                        }
                    },
                    label: {
                        position: {
                            name: "bottom",
                            args: {
                                y: 10
                            },
                        },
                    },
                },
                'out': {
                    position: {
                        name: "bottom",
                    },
                    attrs: {
                        '.port-body': {
                            r: 8,
                            stroke: "#212121",
                            fill: "#ffffff",
                        },
                        '.port-label': {
                            fontSize: 12,
                        },
                    },
                    label: {
                        position: {
                            name: "top",
                            args: {
                                y: -10
                            },
                        },
                    },
                }
            }
        },
        attrs: {
            '.body': {
                rx: 5,
            },
            '.label': {
                'type': 'primary',
                fontSize: 16,
                'ref-x': .5,
                'ref-y': 20
            }
        },
    });

    //const boundaryTool = new joint.elementTools.Boundary();
    //const removeButton = new joint.elementTools.Remove();
    //const toolsView = new joint.dia.ToolsView({
    //  tools: [boundaryTool, removeButton]
    //});

    function genInterfacePr() {
        return gateTemplate.clone().set('outPorts', ['out']).attr('.label/text', 'Interface\nInterface').attr('magnet', true)
            .prop(
                {
                    "@id": "dtmi:itri:build:floor;1",
                    "@type": "Interface",
                    "displayName": "Interface",
                    "@context": "dtmi:dtdl:context;2",
                }
            );
    };

    function genTelemetryPr() {
        return gateTemplate.clone().set('inPorts', ['in']).set('outPorts', ['out']).attr('.label/text', 'Telemetry')
            .prop(
                {
                    "@type": "Telemetry",
                    "displayName": "Telemetry"
                }
            );
    };

    function genPropertyPr() {
        return gateTemplate.clone().set('inPorts', ['in']).set('outPorts', ['out']).attr('.label/text', 'Property')
            .prop(
                {
                    "@type": "Property",
                    "displayName": "Property"
                }
            );
    };

    function genRelationshipPr() {
        return gateTemplate.clone().set('inPorts', ['in ']).set('outPorts', ['out']).attr('.label/text', 'Relationship')
            .prop(
                {
                    "@type": "Relationship",
                    "displayName": "Relationship"
                }
            );
    };

    function genComponentPr() {
        return gateTemplate.clone().set('inPorts', ['in ']).set('outPorts', ['out']).attr('.label/text', 'Component')
            .prop(
                {
                    "@type": "Component",
                    "displayName": "Component"
                }
            );
    };

    function genCommandPr() {
        return gateTemplate.clone().set('inPorts', ['in ']).set('outPorts', ['out']).attr('.label/text', 'Command')
            .prop(
                {
                    "@type": "Command",
                    "displayName": "Command"
                }
            );
    };

    function genCommandTypePr() {
        return gateTemplate.clone().set('inPorts', ['in ']).set('outPorts', ['out']).attr('.label/text', 'CommandType')
            .prop(
                {
                    "@type": "CommandType",
                    "displayName": "CommandType"
                }
            );
    };

    function generateComponentForm(model) {
        const modelJSON = model.toJSON();
        const preForm = document.getElementById('componentForm');
        if (preForm) {
            preForm.remove()
        }
        const form = document.createElement("form");
        form.setAttribute('id', "componentForm")
        form.style.marginLeft = "12px";
        const elementUl = document.createElement("ul");
        elementUl.style.paddingLeft = "0px";

        if (modelJSON["@type"] === "Interface") {
            generateInput("@id", modelJSON["@id"], false);
            generateInput("@context", modelJSON["@context"], true);
        }
        generateInput("displayName", modelJSON.displayName, false);
        generateInput("@type", modelJSON["@type"], true);

        var elementChild = document.createElement("input");
        elementChild.setAttribute('type', "button");
        elementChild.setAttribute('value', "儲存");
        elementChild.setAttribute('onclick', 'saveComponent(model);'); // for FF
        elementChild.onclick = () => saveComponent(model); // for IE

        form.appendChild(elementChild);

        if (modelJSON["@type"] !== "Interface") {
            elementChild = document.createElement("input");
            elementChild.setAttribute('type', "button");
            elementChild.setAttribute('value', "刪除");
            elementChild.setAttribute('onclick', 'deleteComponent(model);');
            elementChild.onclick = () => deleteComponent(model);
        }

        form.appendChild(elementChild);

        document.getElementById('component').appendChild(form);

        function generateInput(label, value, disabled) {
            const elementLabelLi = document.createElement("li");
            const elementLabelChild = document.createTextNode(label);
            const elementChildLi = document.createElement("li");
            const elementChild = document.createElement("input");
            elementLabelLi.setAttribute("type", "none");
            elementLabelLi.setAttribute("for", label);
            elementLabelLi.setAttribute("name", "label");
            elementChildLi.setAttribute("type", "none");
            elementChild.setAttribute('type', "text");
            elementChild.setAttribute('name', label);
            elementChild.setAttribute('value', value);
            if (disabled) {
                elementChild.setAttribute('disabled', disabled);
            }

            elementLabelLi.appendChild(elementLabelChild);
            elementUl.appendChild(elementLabelLi);
            elementChildLi.appendChild(elementChild);
            elementUl.appendChild(elementChildLi);

            form.appendChild(elementUl);
        }
    }

    function saveComponent(component) {
        const form = document.getElementById('componentForm');
        const ul = form.getElementsByTagName('ul')[0];
        const inputs = ul.getElementsByTagName('input');
        const modelJson = component.toJSON();
        for (var index = 0; index < inputs.length; index++) {
            const input = inputs[index];
            if (input.type === "text") {
                component.prop(input.name, input.value);
            }
        }
        component.attr('.label/text', `${modelJson.displayName}\n${modelJson["@type"]}`)
        // console.log(component)
    }

    function deleteComponent(component) {
        component.remove();
        prevSelectElement = null;
    }

    // const delButton = new joint.shapes.standard.TextBlock();
    // delButton.resize(10, 10);
    // delButton.position(0, 0);
    // delButton.attr('label/text', 'X');

    paper.on({
        // 'blank:pointerdown': function(elementView, evt) {
        //     if (this.currentEle) {
        //       console.log(this.currentEle);
        //       this.currentEle.unhighlight();
        //       this.currentEle = null;
        //     }
        // },

        'element:pointerdown': function (elementView, evt) {
            /* 
              // if (this.currentEle) {
              //   console.log(this.currentEle);
              //   this.currentEle.unhighlight();
              //   this.currentEle = null;
              // }
              if (elementView.model.attr('.label/type') == 'primary') {
                // 潛在效能問題
                var type = elementView.model.attr('.label/text');
                if (type == 'Telemetry') {
                  graph.addCell(genTelemetryPr().translate(20, 20));
                } else if (type == 'Property') {
                  graph.addCell(genPropertyPr().translate(20, 120));
                } else if (type == 'Command') {
                  graph.addCell(genCommandPr().translate(20, 220));
                }
                // 挪到图层的上层
                elementView.model.toFront();
              } else if (elementView.model.attr('.label/type') == 'instance') {
                evt.data = elementView.model.position();
                // elementView.highlight();
                // this.currentEle = elementView;
              } 
            */
        },

        'element:pointerup': function (elementView, evt, x, y) {
            // console.log('up')
            // console.log(elementView.model.position())
            // if (elementView.model.attr('.label/type') == 'primary') {
            //   /*
            //   if (elementView.model.position().x > 105) {
            //     var type = elementView.model.attr('.label/text');
            //     if (type == 'Telemetry') {
            //       graph.addCell(genTelemetry().translate(elementView.model.position().x, elementView.model.position().y));
            //     } else if (type == 'Property') {
            //       graph.addCell(genProperty().translate(elementView.model.position().x, elementView.model.position().y));
            //     } else if (type == 'Command') {
            //       graph.addCell(genCommand().translate(elementView.model.position().x, elementView.model.position().y));
            //     }
            //   }
            //   graph.removeCells(elementView.model);
            //   */
            // } else {
            //   if (elementView.model.position().x < 110) {
            //     elementView.model.position(evt.data.x, evt.data.y);
            //   }
            // }
        },

        'element:mouseover': function (elementView, evt) {
            if (elementView.model.attr('.label/type') === 'instance') {
                // elementView.highlight();
                // elementView.model.embed(delButton.clone().translate(elementView.model.position().x, elementView.model.position().y))
            }
        },

        'element:mouseout': function (elementView, evt) {
            if (elementView.model.attr('.label/type') === 'instance') {
                // elementView.unhighlight();
            }
        },

        'element:mouseenter': function (elementView) {
            elementView.showTools();
        },

        'element:mouseleave ': function (elementView) {
            elementView.hideTools();
        },

        'element:pointerclick': function (elementView, evt) {
            if (elementView.model.attr('.label/type') === 'instance') {
                if (prevSelectElement !== null) {
                    prevSelectElement.unhighlight();
                    if (prevSelectElement && prevSelectElement.model && prevSelectElement.model.id == elementView.model.id) {
                        console.log('same')
                    }
                }
                prevSelectElement = elementView;
                elementView.highlight();
                generateComponentForm(elementView.model);
            }
        },

        // 'element:pointerdblclick': function(elementView, evt) {
        //   if (elementView.model.attr('.label/type') === 'instance') {
        //     console.log(elementView.model)
        //     evt.data = elementView.model.position();
        //     elementView.model.remove();
        //   } 
        // },

    });

    stencilContainerPaper.on({
        'cell:pointerdown': function (cellView, e, x, y) {
            $('body').append('<div id="floatHolder" style="position:fixed;z-index:100;opacity:.7;pointer-event:none;"></div>');
            const floatGraph = new joint.dia.Graph;
            const floatPaper = new joint.dia.Paper({
                el: $('#floatHolder'),
                model: floatGraph,
                interactive: false
            });
            const floatShape = cellView.model.clone();
            const pos = cellView.model.position();
            const offset = {
                x: x - pos.x,
                y: y - pos.y
            };
            const componentSize = floatShape.size();
            floatPaper.setDimensions(componentSize.width, componentSize.height);
            floatShape.position(0, 0);
            floatGraph.addCell(floatShape);
            $("#floatHolder").offset({
                left: e.pageX - offset.x,
                top: e.pageY - offset.y
            });
            $('body').on('mousemove.fly', (e) => {
                $("#floatHolder").offset({
                    left: e.pageX - offset.x,
                    top: e.pageY - offset.y
                });
            });
            $('body').on('mouseup.fly', (e) => {
                const x = e.pageX;
                const y = e.pageY;
                const target = paper.$el.offset();

                // Dropped over paper ?
                if (x > target.left && x < target.left + paper.$el.width() && y > target.top && y < target.top + paper.$el.height()) {
                    const s = floatShape.clone().attr('.label/type', 'instance').attr('.label/text', `${floatShape.toJSON().displayName}\n${floatShape.toJSON()["@type"]}`);
                    s.position(x - target.left - offset.x, y - target.top - offset.y);
                    graph.addCell(s);
                }
                $('body').off('mousemove.fly').off('mouseup.fly');
                floatShape.remove();
                $('#floatHolder').remove();
            });
        },
    });

    /*
    graph.addCell(genTelemetryPr().translate(20, 20));
    graph.addCell(genPropertyPr().translate(20, 120));
    graph.addCell(genCommandPr().translate(20, 220));
    */

    stencilContainerGraph.addCells([genTelemetryPr().translate(20, 20), genPropertyPr().translate(20, 120), genRelationshipPr().translate(20, 220), genComponentPr().translate(20, 320)]);
    graph.addCell(genInterfacePr().attr('.label/type', 'instance').translate(50, 50));

    /*
    let separator = new joint.shapes.standard.Polyline();
    separator.resize(5, 400);
    separator.position(95, 0);
    //separator.attr('body/refPoints', '115,0 115,400 120,400 120,0');
    separator.addTo(graph);
    */


    // document.getElementById("mySavedModel").value = JSON.stringify(graph.toJSON());
    document.getElementById("newButton").addEventListener("click", () => {
        graph.fromJSON({ "cells": [] });
        graph.addCell(genInterfacePr().attr('.label/type', 'instance').translate(50, 50));
    });

    document.getElementById("saveButton").addEventListener("click", () => {
        document.getElementById("mySavedModel").value = JSON.stringify(graph.toJSON());
    });

    document.getElementById("loadButton").addEventListener("click", () => {
        try {
            graph.fromJSON(JSON.parse(prompt("請輸入JSON格式", `{"cells":[]}`)));
        } catch (reason) {
            alert("格式錯誤，請重新輸入。");
        }
    });
}