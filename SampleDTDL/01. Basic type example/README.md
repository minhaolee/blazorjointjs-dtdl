# ITRI building DTDL Models

This repository includes DTDL models for ITRI building based on the [DTDL-V2](https://github.com/Azure/opendigitaltwins-dtdl/blob/master/DTDL/v2/dtdlv2.md) standard. These models can be used to create digital twin instances in [Azure Digital Twins](https://azure.microsoft.com/services/digital-twins/) directly or through the [ITRI-ADX](https://gitlab.com/minhaolee) No Code Application Development Platform.  



![](../../res/SimpleBuildingModel.jpeg)

<center>Example ITRI-Building Azure Digital Twin in ITRI-ADX using the open-source DTDL models and data from Azure Digital Twins


![](./res/2022-05-16_170908.jpg)



![](./res/1652676252645.jpg)





![](./res/1652676339334.jpg)

## Contains




### Building Information

| File                 | Description                                 |
| -------------------- | ------------------------------------------- |
| building.json        | Building  general information               |
| floor.json           | Floor information                           |
| room.json            | Room information                            |
| ConferenceRoom.json  | Conference Room information                 |
| sensor.json          | Sensor information                          |
| ReusableTypeAcc.json | ReusableType with accelerometer information |

### Not Supported*

| File                         | Description                                            |
| ---------------------------- | ------------------------------------------------------ |
| building-rel_has_floors.json | Building  version reference with "rel_has_floors.json" |
| Door.json                    | Door Component information                             |

*These files are currently not supported in blazorjointjs-dtdl , but can be used in Azure Digital Twin.

## Support

This repository was created by https://gitlab.com/minhaolee in collaboration with the [Digital Twin Consortium](https://www.digitaltwinconsortium.org). For assistance or feature requests, contact minhaolee@itri.org.tw.

