

# Start Config the JsonServer 

Let's see CLI options of **json-server** package: `$ json-server -h`

```
...
--id, -i   Set database id property (e.g. _id)   [default: "id"]
...
```

The id defined in the DTDL v2 specification id called '**@Id**'

Let's try to start json-server with new id called  '**@Id**'   (for example): `json-server --id @id db-dtdl.json`

   

```powershell
開啟終端機 , 執行必要安裝步驟 step 1, 2, 3  npm , yarn
1. PS > npm init -y
   
2. PS > npm install -g yarn
   
3. PS > yarn add json-server

啟動webapi方法一 :  (無多租戶)
4. PS > yarn server-json

啟動webapi方法二 :  (多租戶分離)
5. PS > yarn multi-tenancy-server-json
```

   

### 匯入 postman  進行 WebAPI CRUD 操作

```powershell
搭配啟動webapi方法一 :  (無多租戶)

ITRI-DTDL API.postman_collection.json


搭配啟動webapi方法二 :  (多租戶分離)

ITRI-DTDL API-Tenancy.postman_collection.json
```






## Start  Query 

check encode and decode in https://www.urlencoder.org/ 

- #### get "@id" =  "dtmi:com:example:floor;1"

```http
http://localhost:4000/Models/dtmi:com:example:floor;1
```



- #### filter  with "@id" =  "dtmi:com:example:floor;1" and "contents.@type" = "Relationship"

```http
http://127.0.0.1:4000/Models?@id=dtmi:com:example:floor;1&contents.@type= Relationship
```



- #### _sort  _order 排序

  - 參數為 asc正數(預設)、 desc倒數

```http
http://127.0.0.1:4000/Models?_sort=contents.schema.enumValues.name&_order=desc
```



- #### pagination 分頁功能

```http
http://127.0.0.1:4000/models?_page=1&limit=2
```



- #### Slice 選取範圍 

  - _start
  - _end 或 _limit

```http
GET /posts?_start=20&_end=30
GET /posts/1/comments?_start=20&_end=30
GET /posts/1/comments?_start=20&_limit=10
```





- #### full text search 篩選資料 / 搜尋全文 (?q)   

  - full text search string  "B1"
  - GET 直接利用網址待入參數
  - ?q=關鍵字 搜尋所有屬性的內容
  - _like 相似於的判斷 (支援 RegExp)

```http
http://127.0.0.1:4000/models?q=B1
```

  

- #### 字串 & 數值大小於

  - _gte 大於等於_
  - lte 小於等於
  - _ne 不等於

```http
GET /posts?views_gte=10&views_lte=20
GET /posts?id_ne=1
GET /posts?title_like=server
```





- #### _embed

```http
http://127.0.0.1:4000/Models?_embed=Twins
```



- #### _expand

```http
http://127.0.0.1:4000/Twins?_expand=Models
```





- #### 產生 random data from javascript not db.json

```
faker.js or casual.js or chance.js
here we use a simple 
```

